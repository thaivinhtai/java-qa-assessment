const data = [
  {
    id: 1,
    firstName: "Lenox",
    lastName: "Lutfi",
    title: "QA Engineer",
    phoneNumber: "123-456-7890",
    email: "lenox.lutfi@foo.bar",
  },
  {
    id: 2,
    firstName: "Jane",
    lastName: "Doe",
    title: "FE Developer",
    phoneNumber: "123-456-7890",
    email: "jane.doe@foo.bar",
  },
  {
    id: 3,
    firstName: "Leo",
    lastName: "Smith",
    title: "Designer",
    phoneNumber: "123-456-7890",
    email: "Leo.Smith@foo.bar",
  },
  {
    id: 4,
    firstName: "Levi",
    lastName: "Hughes",
    title: "BE Developer",
    phoneNumber: "123-456-7890",
    email: "Levi.Hughes@foo.bar",
  },
];
export default data;
