import data from "./data";

type Member = {
  id: number;
  firstName: string;
  lastName: string;
  title: string;
  phoneNumber: string;
  email: string;
};

class DataBase {
  members: Member[] = [];

  constructor() {
    this.members = data;
  }

  getAllMembers(): Member[] {
    return this.shuffleMemberList(this.members);
  }

  getMember(id: number): Member | undefined {
    return this.members.find((member) => member.id === id);
  }

  getMembers(queryString: string): Member[] {
    queryString = queryString.toLowerCase();
    return this.members.filter((member) => {
      return (
        member.id.toString().toLowerCase().includes(queryString) ||
        member.lastName.toLowerCase().includes(queryString) ||
        member.firstName.toLowerCase().includes(queryString) ||
        member.title.toLowerCase().includes(queryString) ||
        member.email.toLowerCase().includes(queryString)
      );
    });
  }

  shuffleMemberList(memberList: Member[]) {
    return memberList
    .map(value => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ value }) => value)
  }

  addMember(member: Member): number {
    // const id = data.length + 1;
    const id = this.members.length + 1;
    member.id = id;
    // data.push(member);
    this.members.push(member);
    return id;
  }

  removeMember(id: number): number {
    let member: Member | undefined = this.getMember(id);
    if (typeof member === "undefined") {
      return -1;
    }
    this.members = this.members.filter(memberToRemove => member.id !== memberToRemove.id);
    return member.id;
  }

  updateMemberInfo(id: number, data: object): Member | undefined {
    let member: Member | undefined = this.getMember(id);
    if (typeof member === "undefined") {
      return undefined;
    }
    member = Object.assign({}, member, Object.keys(data)
        .filter(key => member[key] !== undefined && member[key] !== null)
        .reduce((obj, key) => {
          obj[key] = data[key];
          return obj;
        }, {})
    );
    this.members = this.members.map(member_ => {
      if (member_.id === id) {
        return member;
      }
      return member_;
    })
    return member;
  }
}

const database = new DataBase();
export default database;
