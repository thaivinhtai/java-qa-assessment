import { Request, Response } from "express";
import database from "../database";

export default function updateMember(req: Request, res: Response) {
  console.log(req);
  const member = database.updateMemberInfo(Number(req.params.id), req.body);
  if (typeof member !== "undefined") {
    res.send(JSON.stringify(member));
  } else {
    res.sendStatus(404);
  }
}
