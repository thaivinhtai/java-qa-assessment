import { Request, Response } from "express";
import database from "../database";

export default function removeMember(req: Request, res: Response) {
  const id = database.removeMember(Number(req.params.id));
  if (id != -1) {
    res.send(JSON.stringify({
      "message": `User ${id} has been deleted.`
    }));
  } else {
    res.sendStatus(404);
  }
}
