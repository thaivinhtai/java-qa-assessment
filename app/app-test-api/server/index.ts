import express, { Request, Response } from "express";
import getMember from "../services/getMember";
import getMembers from "../services/getMembers";
import addMember from "../services/addMember";
import removeMember from "../services/removeMember";
import cors from "cors";
import updateMember from "../services/updateMember";

import swaggerUI from "swagger-ui-express";
import swaggerDocument from "./swagger";

const app = express();

const BE_PORT = process.env.BE_PORT || 3000;
const BE_HOST = process.env.BE_HOST || '0.0.0.0';
const FE_HOST = process.env.FE_HOST || '0.0.0.0';
const FE_PORT = process.env.FE_PORT || '8080';

const PREFIX_URL = process.env.PREFIX_URL || "";

const allowedOrigins = [
    `http://${FE_HOST}:${FE_PORT}`,
    `http://${BE_HOST}:${BE_PORT}`,
    `http://localhost:${FE_PORT}`,
    `http://localhost:${BE_PORT}`,
    `http://127.0.0.1:${FE_PORT}`,
    `http://127.0.0.1:${BE_PORT}`,
    `http://0.0.0.0:${FE_PORT}`,
    `http://0.0.0.0:${BE_PORT}`,
    `http://127.0.0.1:8080`,
    `http://0.0.0.0:8080`,
    `http://localhost:8080`,
];

const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors(options));
app.use(express.json());
app.use(PREFIX_URL + '/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.get(PREFIX_URL + "/", (_req: Request, res: Response) => {
  // res.send("--App Test API--");
  res.redirect(PREFIX_URL + '/api-docs');
});

app.get(PREFIX_URL + "/members/:id", getMember);

app.get(PREFIX_URL + "/members", getMembers);

app.post(PREFIX_URL + "/members", addMember);

app.post(PREFIX_URL + "/members/:id", updateMember);

app.delete(PREFIX_URL + "/members/:id", removeMember);

app.listen(BE_PORT, () => {
  console.log(`Listening on port ${BE_PORT}`);
});
