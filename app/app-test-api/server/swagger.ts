const swaggerDocument = {
  "openapi": "3.0.3",
  "info": {
    "title": "App Test API - OpenAPI 3.0 - Env: " + (process.env.TEST_ENV || "QA"),
    "description": "This is a sample App Test based on the OpenAPI 3.0 specification.",
    "contact": {
      "email": "thaivinhtai@gmail.com"
    },
    "version": "1.0.0"
  },
  "externalDocs": {
    "description": "Tai Thai LinkedIn",
    "url": "https://www.linkedin.com/in/taivinhthai/"
  },
  "servers": [
    {
      "url":"http://" + (process.env.BE_HOST || "0.0.0.0") + ":" + (process.env.BE_PORT || 3000) + "/"
    }
  ],
  "paths": {
    [(process.env.PREFIX_URL || "") + "/members"]: {
      "get": {
        "summary": "Get members",
        "description": "Get all members as a list",
        "operationId": "getMembers",
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/MemberArray"
                }
              }
            }
          }
        }
      },
      "post": {
        "summary": "Add a new member",
        "description": "Add a new member to the list",
        "operationId": "addMember",
        "requestBody": {
          "description": "Add a new member to the list",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/MemberWithoutId"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/text": {
                "schema": {
                  "$ref": "#/components/schemas/ApiResponse"
                }
              }
            }
          }
        }
      }
    },
    [(process.env.PREFIX_URL || "") + "/members/{memberId}"]: {
      "get": {
        "summary": "Find member by ID",
        "description": "Returns a single member",
        "operationId": "getMember",
        "parameters": [
          {
            "name": "memberId",
            "in": "path",
            "description": "ID of member to return",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Member"
                }
              }
            }
          },
          "404": {
            "description": "Member not found"
          }
        }
      },
      "post": {
        "summary": "Updates a member in the list with json data",
        "description": "Updates a member in the list with json data",
        "operationId": "updateMember",
        "parameters": [
          {
            "name": "memberId",
            "in": "path",
            "description": "ID of member that needs to be updated",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "requestBody": {
          "description": "Update a member from the list",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/MemberWithoutId"
              }
            }
          },
          "required": true
        },
        "responses": {
          "404": {
            "description": "Member not found"
          }
        }
      },
      "delete": {
        "summary": "Deletes a member",
        "description": "delete a member",
        "operationId": "deleteMember",
        "parameters": [
          {
            "name": "memberId",
            "in": "path",
            "description": "Member id to delete",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int64"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/DeleteResponse"
                }
              }
            }
          },
          "404": {
            "description": "Member not found"
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Member": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "format": "int64",
            "example": 1
          },
          "firstName": {
            "type": "string",
            "example": "Tai"
          },
          "lastName": {
            "type": "string",
            "example": "Thai"
          },
          "title": {
            "type": "string",
            "example": "QA Engineer"
          },
          "phoneNumber": {
            "type": "string",
            "example": "123-456-7890"
          },
          "email": {
            "type": "string",
            "example": "tai.thai@foo.bar"
          }
        }
      },
      "MemberWithoutId": {
        "type": "object",
        "properties": {
          "firstName": {
            "type": "string",
            "example": "Tai"
          },
          "lastName": {
            "type": "string",
            "example": "Thai"
          },
          "title": {
            "type": "string",
            "example": "QA Engineer"
          },
          "phoneNumber": {
            "type": "string",
            "example": "123-456-7890"
          },
          "email": {
            "type": "string",
            "example": "tai.thai@foo.bar"
          }
        }
      },
      "MemberArray": {
        "type": "array",
        "items": {
          "$ref": "#/components/schemas/Member"
        }
      },
      "ApiResponse": {
        "type": "string",
        "example": "success"
      },
      "DeleteResponse": {
        "type": "object",
        "properties": {
          "message": {
            "type": "string",
            "example": "Success"
          }
        }
      }
    }
  }
}

export default swaggerDocument;
