type Member = {
  id: number;
  firstName: string;
  lastName: string;
  title: string;
  phoneNumber: string;
  email: string;
};
