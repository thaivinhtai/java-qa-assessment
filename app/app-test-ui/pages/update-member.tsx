import type { NextPage } from "next";
import { useState } from "react";
import SearchBar from "../components/SearchBar";
import { EndPoints } from "../config";

const Home: NextPage = () => {
    const [member, updateMember] = useState({
        firstName: "",
        lastName: "",
        title: "",
        phoneNumber: "",
        email: "",
    } as Member);


    const [id, updateId] = useState("");

    const [loading, setLoading] = useState(true);

    const [memberId, setMemberId] = useState("");

    const [deleteMessage, setDeleteMessage] = useState("");

    const [action, setAction] = useState("");

    async function getMember(event: { preventDefault: () => void }) {
        event.preventDefault();

        const response = await fetch(EndPoints.GetMember.replace(":id", id));
        if (response.ok) {
            const result = await response.json();
            updateMember(result);
            setLoading(false);
        } else {
            setLoading(true);
        }
    }

    async function updateMemberInfo(event: { preventDefault: () => void }) {
        event.preventDefault();
        if (action == "update") {
            const response = await fetch(EndPoints.UpdateMember.replace(":id", id), {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(member),
            });
            const result = await response.json();
            setMemberId(result.id);
        } else {
            const response = await fetch(EndPoints.UpdateMember.replace(":id", id), {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                }
            });
            const result = await response.json();
            setMemberId(id);
            setDeleteMessage(result.message);
        }
    }

    return (
        <div className="px-10 py-10">
            <SearchBar searchFunction={getMember} updateInput={updateId} placeholder="ID"/>
            {!loading ? (
                <form className="p-10" onSubmit={updateMemberInfo}>
                    <div className="grid gap-6 mb-6 md:grid-cols-2">
                        <div>
                            <label
                                htmlFor="first_name"
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                            >
                                First name*
                            </label>
                            <input
                                type="text"
                                id="first_name"
                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder={member.firstName}
                                value={member.firstName}
                                onChange={(e) =>
                                    updateMember({ ...member, firstName: e.target.value })
                                }
                                required
                            />
                        </div>
                        <div>
                            <label
                                htmlFor="last_name"
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                            >
                                Last name*
                            </label>
                            <input
                                type="text"
                                id="last_name"
                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder={member.lastName}
                                value={member.lastName}
                                onChange={(e) =>
                                    updateMember({ ...member, lastName: e.target.value })
                                }
                                required
                            />
                        </div>
                        <div>
                            <label
                                htmlFor="title"
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                            >
                                Title*
                            </label>
                            <input
                                type="text"
                                id="title"
                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder={member.title}
                                value={member.title}
                                onChange={(e) =>
                                    updateMember({ ...member, title: e.target.value })
                                }
                                required
                            />
                        </div>
                        <div>
                            <label
                                htmlFor="phone"
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                            >
                                Phone number
                            </label>
                            <input
                                type="tel"
                                id="phone"
                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder={member.phoneNumber}
                                value={member.phoneNumber}
                                onChange={(e) =>
                                    updateMember({ ...member, phoneNumber: e.target.value })
                                }
                            />
                        </div>
                    </div>
                    <div className="mb-6">
                        <label
                            htmlFor="email"
                            className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                        >
                            Email address
                        </label>
                        <input
                            type="email"
                            id="email"
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            placeholder={member.email}
                            value={member.email}
                            onChange={(e) =>
                                updateMember({ ...member, email: e.target.value })
                            }
                            required
                        />
                    </div>
                    <div className="grid gap-6 mb-6 md:grid-cols-2">
                        <button
                            type="submit"
                            onClick={(e) => setAction("update")}
                            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        >
                            Update
                        </button>
                        <button
                            type="submit"
                            onClick={(e) => setAction("delete")}
                            className="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                        >
                            Delete
                        </button>
                    </div>
                    {!!memberId && (
                        <div
                            id="toast-success"
                            className="flex items-center p-4 mb-4 w-full max-w-xs text-gray-500 bg-white rounded-lg shadow dark:text-gray-400 dark:bg-gray-800"
                            role="alert"
                        >
                            <div className="inline-flex flex-shrink-0 justify-center items-center w-8 h-8 text-green-500 bg-green-100 rounded-lg dark:bg-green-800 dark:text-green-200">
                                <svg
                                    aria-hidden="true"
                                    className="w-5 h-5"
                                    fill="currentColor"
                                    viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                        clipRule="evenodd"
                                    ></path>
                                </svg>
                                <span className="sr-only">Check icon</span>
                            </div>
                            <div className="ml-3 text-sm font-normal">
                                Member has been {!deleteMessage ? 'updated' : 'deleted'} - ID: {memberId}
                            </div>
                            <button
                                type="button"
                                className="ml-auto -mx-1.5 -my-1.5 bg-white text-gray-400 hover:text-gray-900 rounded-lg focus:ring-2 focus:ring-gray-300 p-1.5 hover:bg-gray-100 inline-flex h-8 w-8 dark:text-gray-500 dark:hover:text-white dark:bg-gray-800 dark:hover:bg-gray-700"
                                data-dismiss-target="#toast-success"
                                aria-label="Close"
                            >
                                <span className="sr-only">Close</span>
                                <svg
                                    aria-hidden="true"
                                    className="w-5 h-5"
                                    fill="currentColor"
                                    viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                        clipRule="evenodd"
                                    ></path>
                                </svg>
                            </button>
                        </div>
                    )}
                </form>
            ) : (
                (<h1 className="px-1 py-10 text-3xl">Nothing to display... </h1>)
            )}
        </div>
    );
};

export default Home;
