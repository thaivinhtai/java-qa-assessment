import type { AppProps } from "next/app";
import { useRouter } from 'next/router'
import Layout from "../components/Layout";
import "../styles/globals.css";

function AppTestUI({ Component, pageProps }: AppProps) {
    // const router = useRouter()
    // const prefix: string = process.env.NEXT_PUBLIC_APP_PREFIX_URL || '';
    // console.log("prefix" + prefix);
    //
    // // Modify the router object to add the prefix to all URLs
    // if (router.asPath.startsWith(prefix)) {
    //     router.asPath = router.asPath.slice(prefix.length)
    //     router.pathname = router.pathname.slice(prefix.length)
    // }

    return (
    <Layout title="Test UI Profile Manager">
      <Component {...pageProps} />
    </Layout>
    );
}

export default AppTestUI;
