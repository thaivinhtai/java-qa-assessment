const HOST = process.env.NEXT_PUBLIC_BE_HOST || '0.0.0.0';
const API_PORT = process.env.NEXT_PUBLIC_BE_PORT || '3000';
const API_PREFIX_URL = process.env.NEXT_PUBLIC_API_PREFIX_URL || '';

export const ApiServer = `http://${HOST}:${API_PORT}${API_PREFIX_URL}`;
console.log(ApiServer);

export const EndPoints = {
  GetMembers: `${ApiServer}/members`,
  GetMember: `${ApiServer}/members/:id`,
  UpdateMember: `${ApiServer}/members/:id`,
  AddMember: `${ApiServer}/members`,
};
