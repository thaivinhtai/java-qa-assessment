# Automation Testing Framework With Python

## Table of Contents
- [Assessment overview](#Assessment overview)
  - [Overall test automation framework design](#Overall test automation framework design)
  - [Ability to run the test scripts across multiple environments](#Ability to run the test scripts across multiple environments)
  - [Ability to run the test scripts across multiple browsers](#Ability to run the test scripts across multiple browsers)
  - [Test reporting capability](#Test reporting capability)
  - [Test scenarios coverage](#Test scenarios coverage)
  - [Bugs/enhancements identified](#Bugs/enhancements identified)
- [Set up the Automation Framework](#Set up the Automation Framework)
  - [Pre-requisites](#Pre requisites)
  - [Set up the dependencies](#Set up the dependencies)
- [Automation Framework Usage](#Automation Framework Usage)
  - [Running in Debug Mode](#Running in Debug Mode)
  - [Running with different browsers](#Running with different browsers)
  - [Running test cases based on Priority](#Running test cases based on Priority)
    - [Run high priority test cases only](#Run high priority test cases only)
    - [Run high and medium test cases](#Run high and medium test cases)
    - [Run test cases at all priority level](#Run test cases at all priority level)
  - [Generate test cases document](#Generate test cases document)
  - [Push result to Allure report server](#Push result to Allure report server)
  - [More features](#More features)

## Assessment overview
### Overall test automation framework design

The automation framework has the following overall structure:

```
root_dir
    |
    |----- sqa_engine
    |          |
    |          |--------- allure_config       # Config to communicate with the Allure report server
    |          |--------- automation_lilbs    # Low-level automation libs, which can be reusable
    |          |--------- general_config      # Some other configs, for ex: The connect to the Test Management System
    |          |--------- tools               # Tools support automation, in this case is Allure
    |          |--------- utilities           # All libs to manage the framework, include: generating logs, push result, handle webdriver, browsers....
    |          |--------- main.py             # The process starts here.
    |
    |----- sqa_web          
    |          |
    |          |----- libs                    # High-level libs for specified test module, for ex: page_object_model
    |          |----- test_design             # Test cases are stored here
    |          
    |----- sqa_resource          
    |          |
    |          |-------- environments         # This package deals with multiple env
    |          |-------- common_variables.py  # Some common vars for testing
    |          
    |----- <packages just like the sqa_web if the project scales out>          
```


### Ability to run the test scripts across multiple environments

By using the dynamic import of Python, we can manage as many environments as we want. The following is the sqa_resource/environments structure.

```
environments
    |-------- dev
    |-------- qa
```

We just need to import the target package corresponding to the environment.

### Ability to run the test scripts across multiple browsers

In this project, we can run tests on Chrome, Firefox, Safari, Edge, and Brave without caring about the webdriver setup. The framework itself deals with that.

### Test reporting capability

The framework supports run Allure report locally as well as push the report to the online server.
It also supports linking issues or bugs between the Allure report and the Test Management System.

### Test scenarios coverage

Please visit my Notion for the [Trace-matrix](https://tai-thai-demo.notion.site/Trace-matrix-5b5eac73ccf94fdca0cc914b88c77e47) and the [test list](https://tai-thai-demo.notion.site/80311a8260e04594963b707a70632321?v=4ead37b91d944d74a5aaa96a8768b7dd).

These two pages are linked to the Allure Report.

## Set up the Automation Framework
### Pre requisites
- Python 3.8 or above https://www.python.org/
- Java Runtime Environment (JRE)

### Set up the dependencies

1. Create the Python Virtual Environment:
   ```shell
   cd testing_framework
   python -m venv .venv
   ```
2. Access the Virtual Environment:
   - macOS/Linux:
      ```shell
      source .venv/bin/activate
      ```
   - Windows:
     ```shell
      .\.venv\Scripts\activate
     ```
3. Install the requirements:
   ```shell
   pip install -r requirements.txt
   ```
   
## Automation Framework Usage
### Quick start
```shell
cd testing_framework
python sqa_engine/main.py --run-allure --debug
```

### Running in Debug Mode

By adding the '--debug' flag, the scripts will run in debug mode. This means the logging is more detailed, and the web will run in UI mode. By default, the web will run in headless mode.

***Note:*** **You can not push the test result to the online Allure service when debug mode is on.**

### Running with different browsers

The project currently supports the following browsers: Chrome, Firefox, Edge, Safari, and Brave.

By adding the '--browser' flag and the browser name as above, the tests can be conducted on the target browser.

For example:
```shell
python sqa_engine/main.py --browser edge --run-allure --debug
```

If there is no '--browser' flag, the Chrome browser will be used by default.

### Running test cases based on Priority
#### Run high priority test cases only
```shell
python sqa_engine/main.py -p high --run-allure --debug
```

Medium and Low priority test cases work the same the High priority

#### Run high and medium test cases
```shell
python sqa_engine/main.py -p high medium --run-allure --debug
```

#### Run test cases at all priority level

By default, all test cases should be run.
```shell
python sqa_engine/main.py -p all --run-allure --debug
```

### Generate test cases document
```shell
python sqa_engine/main.py --gen-scripts
```

### Push result to Allure report server

If you run the setup script in the app/start_all, the Allure URL should be: http://localhost:8080/allure/allure-docker-service-ui

1. Access the Allure server and create a project
2. Push the testing result to the project with the following command:
   ```shell
    python sqa_engine/main.py --push-result <project_name>
   ```
   
### More features

Fore more features of the framework, please review the file ***sqa_engine/utilities/args_handler/args_parser.py***
