#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""


from .based import BasedPage


class ViewMember(BasedPage):
    def __init__(self):
        super().__init__()
        self.query_textbox = 'xpath=//input[@type="search"]'
        self.search_button = 'xpath=//button[@type="submit"]'
        self.no_result_message = \
            'xpath=//h1[contains(text(), "Nothing to display...")]'
        self.first_name_field = 'xpath=//label[@id="first_name"]'
        self.last_name_field = 'xpath=//label[@id="last_name"]'
        self.title_field = 'xpath=//label[@id="title"]'
        self.phone_number_field = 'xpath=//label[@id="phone"]'
        self.email_address_field = 'xpath=//label[@id="email"]'

    def query_by_id(self, id_: str) -> dict:
        self.driver.logger.info(f"Query by id : {id_}")
        self._send_string_to_locator(locator=self.query_textbox,
                                     str_to_be_sent=id_)
        self._click_on_locator(self.search_button)
        if self.driver.wait_explicit(
            condition="visibility_of_element_located",
            locator=self.no_result_message
        ):
            return {
                "message": self.driver.get_element(
                    self.no_result_message).text
            }
        return {
            key_: value_ for key_, value_ in zip(
                ("id", "firstName", "lastName", "title",
                 "phoneNumber", "email"),
                (id_, self.driver.get_element(self.first_name_field).text,
                 self.driver.get_element(self.last_name_field).text,
                 self.driver.get_element(self.title_field).text,
                 self.driver.get_element(self.phone_number_field).text,
                 self.driver.get_element(self.email_address_field).text)
            )
        }
