#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

from .based import BasedPage


class HeaderMenu(BasedPage):
    def __init__(self):
        super().__init__()
        self.home_link = 'xpath=//a[text()="Home"]'
        self.add_member_link = 'xpath=//a[text()="Add Member"]'
        self.search_member_link = 'xpath=//a[text()="Search Member"]'
        self.view_member_link = 'xpath=//a[text()="View Member"]'

    def navigate_to_home_page(self):
        self.driver.logger.info("Navigate to Home page.")
        self._click_on_locator(self.home_link)

    def navigate_to_add_member_page(self):
        self.driver.logger.info("Navigate to Add Member page.")
        self._click_on_locator(self.add_member_link)

    def navigate_to_search_member_page(self):
        self.driver.logger.info("Navigate to Search Member page.")
        self._click_on_locator(self.search_member_link)

    def navigate_to_view_member_page(self):
        self.driver.logger.info("Navigate to View Member page.")
        self._click_on_locator(self.view_member_link)
