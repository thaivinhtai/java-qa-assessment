#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

from selenium.common.exceptions import NoSuchWindowException, \
    WebDriverException

from sqa_resource.environments import WEB_URL
from .based import BasedPage


class HomePage(BasedPage):
    def __init__(self):
        super().__init__()
        self.body_rows = 'xpath=//tbody/tr'

    def access_home_page_url(self):
        try:
            self.driver.go_to_url(WEB_URL)
        except (NoSuchWindowException, WebDriverException):
            self.driver.close_session()
            self.access_home_page_url()

    def get_list_members(self) -> list:
        table_rows = self.driver.get_elements(self.body_rows)
        list_members = []
        for row in table_rows:
            list_members.append({
                key_: cell.text for key_, cell in zip(
                    ('id', 'name', 'title', 'email'),
                    row.find_elements("xpath", ".//*"))
            })
        for index, members in enumerate(list_members):
            fist_name, last_name = \
                tuple(list_members[index].pop("name").split())
            list_members[index]["firstName"] = fist_name
            list_members[index]["lastName"] = last_name
        return list_members
