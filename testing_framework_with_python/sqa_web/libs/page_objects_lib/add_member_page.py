#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""


from .based import BasedPage


class AddMember(BasedPage):
    def __init__(self):
        super().__init__()
        self.success_popup = \
            'xpath=//div[contains(text(), "Member has been added")]'
        self.success_popup_close_button = 'xpath=//button[@aria-label="Close"]'
        self.first_name_field = 'xpath=//*[@id="first_name"]'
        self.last_name_field = 'xpath=//*[@id="last_name"]'
        self.title_field = 'xpath=//*[@id="title"]'
        self.phone_number_field = 'xpath=//*[@id="phone"]'
        self.email_address_field = 'xpath=//*[@id="email"]'
        self.agree_checkbox = 'xpath=//*[@id="remember"]'
        self.submit_button = 'xpath=//button[@type="submit"]'

    def add_member(self, first_name: str, last_name: str,
                   title: str, email: str, **kwargs) -> str:
        self.driver.logger.info("Adding member...")
        for field, data in zip(
                (self.first_name_field, self.last_name_field, self.title_field, self.email_address_field),
                (first_name, last_name, title, email)
        ):
            self._send_string_to_locator(locator=field, str_to_be_sent=data)

        if kwargs.get('phone'):
            self._send_string_to_locator(locator=self.phone_number_field,
                                         str_to_be_sent=kwargs.get('phone'))
        if not kwargs.get('not_agree'):
            self._click_on_locator(locator=self.agree_checkbox)
        self._click_on_locator(locator=self.submit_button)
        if self.driver.wait_explicit(
            condition="visibility_of_element_located",
            locator=self.success_popup
        ):
            return self.driver.get_element(
                self.success_popup).text.split(": ")[-1]
        return ""

    def close_success_popup(self):
        self.driver.logger.info("Close the success message popup.")
        self._click_on_locator(self.success_popup_close_button)
        if self.driver.wait_explicit(
            condition="visibility_of_element_located",
            locator=self.success_popup
        ):
            return False
        return True
