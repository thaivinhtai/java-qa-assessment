#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""


from sqa_engine.automation_libs.api_util import \
    send_request_and_receive_response
from sqa_resource.environments import API_HOST, API_PREFIX


class Backend:
    def __init__(self):
        self.host = API_HOST
        self.members_endpoint = API_PREFIX + '/members'

    def get_all_members(self) -> list:
        response = send_request_and_receive_response(
            method="GET", uri=self.host+self.members_endpoint
        )
        if response.get("status_code") == 200:
            return response.get('message')
        return []

    def get_member_by_id(self, id_: str) -> dict:
        response = send_request_and_receive_response(
            method="GET", uri=self.host + self.members_endpoint + '/' + id_
        )
        if response.get("status_code") == 200:
            return response.get('message')
        return {}
