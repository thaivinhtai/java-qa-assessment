#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

from behave import given, when, then, use_step_matcher, step

from sqa_web.libs.api.backend import Backend
from sqa_web.libs.page_objects_lib import HomePage, HeaderMenu, AddMember, \
    ViewMember, SearchMember
from sqa_engine.automation_libs.common_funcs import expect_true, random_str, \
    random_number, choice

from sqa_resource.common_variables import NEW_USER_FULL_INFO, \
    NEW_USER_REQUIRED_INFO, DEFAULT_USER


use_step_matcher("re")


@given("The user gets the list of members from the API")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.list_member_from_api = Backend().get_all_members()


@when("The user accesses the home page and checks the member list")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    HomePage().access_home_page_url()
    context.list_member_from_ui = HomePage().get_list_members()

@then(
    "The members list on the home page should match the data that the user gets from the API")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """

    def check_member(mem_1, mem_2):
        if member_api.get('id') == member_ui.get('id'):
            for key_mem_2 in mem_2.keys():
                expect_true(
                    mem_1.get(key_mem_2) == mem_2.get(key_mem_2),
                    failed_message=f"Data {mem_2.get(key_mem_2)} is incorrect."
                )

    expect_true(
        len(context.list_member_from_api) == len(context.list_member_from_ui),
        failed_message="The numbers of API and UI members are incorrect."
    )
    for member_api in context.list_member_from_api:
        for member_ui in context.list_member_from_ui:
            check_member(member_ui, member_api)


@given("The user accesses the Add Member page")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    HomePage().access_home_page_url()
    HeaderMenu().navigate_to_add_member_page()


@when("The user fills all the fields with valid data")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.new_member_id = AddMember().add_member(
        first_name=NEW_USER_FULL_INFO.get("firstName"),
        last_name=NEW_USER_FULL_INFO.get("lastName"),
        title=NEW_USER_FULL_INFO.get("title"),
        phone=NEW_USER_FULL_INFO.get("phoneNumber"),
        email=NEW_USER_FULL_INFO.get("email")
    )


@then("A new member should be added to the database with the correct data")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    new_member_from_api = Backend().get_member_by_id(context.new_member_id)
    new_member_from_api.pop("id")
    for key_ in new_member_from_api.keys():
        expect_true(
            new_member_from_api[key_] == NEW_USER_FULL_INFO[key_],
            failed_message=f"Data {NEW_USER_FULL_INFO[key_]} is incorrect"
        )


@step(
    "The created member information should be accessible via the View member page")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    HeaderMenu().navigate_to_view_member_page()
    new_member_from_ui = ViewMember().query_by_id(context.new_member_id)
    new_member_from_ui.pop("id")
    for key_ in new_member_from_ui.keys():
        expect_true(
            new_member_from_ui[key_] == NEW_USER_FULL_INFO[key_],
            failed_message=f"Data {NEW_USER_FULL_INFO[key_]} is incorrect"
        )


@when("The user fills in all the minimally required fields with valid data")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.new_member_id = AddMember().add_member(
        first_name=NEW_USER_REQUIRED_INFO.get("firstName"),
        last_name=NEW_USER_REQUIRED_INFO.get("lastName"),
        title=NEW_USER_REQUIRED_INFO.get("title"),
        email=NEW_USER_REQUIRED_INFO.get("email")
    )


@then(
    "A new member should be added to the database with the correct required data")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    new_member_from_api = Backend().get_member_by_id(context.new_member_id)
    new_member_from_api.pop("id")
    for key_ in new_member_from_api.keys():
        expect_true(
            new_member_from_api.get(key_) ==
            NEW_USER_REQUIRED_INFO.get(key_, ""),
            failed_message=
            f"Data {NEW_USER_REQUIRED_INFO.get(key_)} is incorrect"
        )


@step(
    "The newly created member with minimal information should be accessible via the View member page")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    HeaderMenu().navigate_to_view_member_page()
    new_member_from_ui = ViewMember().query_by_id(context.new_member_id)
    new_member_from_ui.pop("id")
    for key_ in new_member_from_ui.keys():
        expect_true(
            new_member_from_ui.get(key_) ==
            NEW_USER_REQUIRED_INFO.get(key_, ""),
            failed_message=
            f"Data {NEW_USER_REQUIRED_INFO.get(key_)} is incorrect"
        )


@when(
    "The user fills all the fields with valid data except a space in (?P<field>.+)")
def step_impl(context, field):
    """
    Parameters
    ----------
    context : behave.runner.Context
    field : str
    """
    test_data = dict(NEW_USER_FULL_INFO)
    test_data[field] = " "
    context.new_member_id = AddMember().add_member(
        first_name=test_data.get("firstName"),
        last_name=test_data.get("lastName"),
        title=test_data.get("title"),
        phone=test_data.get("phoneNumber"),
        email=test_data.get("email")
    )


@then("A new member should not be added")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    expect_true(
        not context.new_member_id,
        failed_message=
        "The new user could be added with a space in the required data",
        defect_code="BUG-069", instance=AddMember()
    )


@when(
    "The user fills all the fields with valid data except empty in (?P<field>.+)")
def step_impl(context, field):
    """
    Parameters
    ----------
    context : behave.runner.Context
    field : str
    """
    test_data = dict(NEW_USER_FULL_INFO)
    test_data[field] = ""
    context.new_member_id = AddMember().add_member(
        first_name=test_data.get("firstName"),
        last_name=test_data.get("lastName"),
        title=test_data.get("title"),
        phone=test_data.get("phoneNumber"),
        email=test_data.get("email")
    )


@when(
    "The user fills all the fields with valid data except the invalid one in (?P<field>.+)")
def step_impl(context, field):
    """
    Parameters
    ----------
    context : behave.runner.Context
    field : str
    """
    test_data = dict(NEW_USER_FULL_INFO)
    test_data[field] = random_str(random_number(1, 20))
    context.new_member_id = AddMember().add_member(
        first_name=test_data.get("firstName"),
        last_name=test_data.get("lastName"),
        title=test_data.get("title"),
        phone=test_data.get("phoneNumber"),
        email=test_data.get("email")
    )


@when(
    "The user fills all the fields with valid data but does not check the agree-checkbox")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.new_member_id = AddMember().add_member(
        first_name=NEW_USER_FULL_INFO.get("firstName"),
        last_name=NEW_USER_FULL_INFO.get("lastName"),
        title=NEW_USER_FULL_INFO.get("title"),
        phone=NEW_USER_FULL_INFO.get("phoneNumber"),
        email=NEW_USER_FULL_INFO.get("email"),
        not_agree=True
    )


@when(
    "The user successfully adds a member and clicks on the success message popup")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.execute_steps(u'''
        When The user fills all the fields with valid data
        ''')
    context.close_popup = AddMember().close_success_popup()


@then("The popup should be closed")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    expect_true(
        context.close_popup,
        failed_message="Can not close the popup.",
        instance=AddMember()
    )


@given("The user accesses the Search Member page")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    HomePage().access_home_page_url()
    HeaderMenu().navigate_to_search_member_page()


@when("The user searches with an existing query")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.query_string = choice(list(DEFAULT_USER.values()))
    context.member_search_result = SearchMember().search(context.query_string)


@then(
    "The page should return a table of members, each of which contains the query string in the info")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    for member in context.member_search_result:
        correct = False
        for data_ in member.values():
            if context.query_string in data_:
                correct = True
                break
        expect_true(
            correct,
            failed_message=
            "There is a record that dose not contain the query.",
            instance=SearchMember()
        )


@when("The user searches with a non-existing query")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.member_search_result = SearchMember().search(
        random_str(20)
    )


@then("The page should return an empty table")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    expect_true(not context.member_search_result)


@when("The user clicks on the search button without input the query")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.member_search_result = SearchMember().search("")


@then("The search function should not work")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.execute_steps(u'''
        Then The page should return an empty table
        ''')


@given("The user accesses the View Member page")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    HomePage().access_home_page_url()
    HeaderMenu().navigate_to_view_member_page()


@when("The user searches with a non-existing ID")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    context.query_result = ViewMember().query_by_id(random_str(20))


@then("The page should return nothing")
def step_impl(context):
    """
    Parameters
    ----------
    context : behave.runner.Context
    """
    expect_true("Nothing" in context.query_result.get("message"))
