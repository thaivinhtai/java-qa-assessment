# Created by thaivinhtai at 15/01/2023
@allure.epic:WEB
Feature: Profile Manager
  """
  Feature description here
  """

  @regression
  Scenario Outline: WEB_ProfMgr_Func_01 - Verify the list of members on the homepage -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=high
    min_version=0.0.1
    max_version=None
    """
    Given The user gets the list of members from the API
    When The user accesses the home page and checks the member list
    Then The members list on the home page should match the data that the user gets from the API

    Examples:
      | browser  |
      |    .     |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_02 - Verify adding a new member with full data -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=high
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Add Member page
    When The user fills all the fields with valid data
    Then A new member should be added to the database with the correct data
      And The created member information should be accessible via the View member page

    Examples:
      | browser  |
      |    .     |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_03 - Verify adding a new member with the minimal required data -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=high
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Add Member page
    When The user fills in all the minimally required fields with valid data
    Then A new member should be added to the database with the correct required data
      And The newly created member with minimal information should be accessible via the View member page

    Examples:
      | browser  |
      |    .     |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_04 - Verify adding a new member with only a space in the <field> -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=medium
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Add Member page
    When The user fills all the fields with valid data except a space in <field>
    Then A new member should not be added

    Examples:
      | browser  |  field      |
      |    .     |  firstName  |
      |    .     |  lastName   |
      |    .     |  title      |
      |    .     |  email      |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_05 - Verify adding a new member with empty data in the <field> -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=high
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Add Member page
    When The user fills all the fields with valid data except empty in <field>
    Then A new member should not be added

    Examples:
      | browser  |  field      |
      |    .     |  firstName  |
      |    .     |  lastName   |
      |    .     |  title      |
      |    .     |  email      |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_06 - Verify adding a new member with invalid data in the <field> -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=medium
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Add Member page
    When The user fills all the fields with valid data except the invalid one in <field>
    Then A new member should not be added

    Examples:
      | browser  |  field       |
      |    .     |  phoneNumber |
      |    .     |  email       |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_07 - Verify adding a new member without checking on the agree-checkbox -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=medium
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Add Member page
    When The user fills all the fields with valid data but does not check the agree-checkbox
    Then A new member should not be added

    Examples:
      | browser  |
      |    .     |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_08 - Verify closing the success message popup after adding a new member successfully -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=medium
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Add Member page
    When The user successfully adds a member and clicks on the success message popup
    Then The popup should be closed

    Examples:
      | browser  |
      |    .     |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_09 - Verify searching members using an existing query -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=high
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Search Member page
    When The user searches with an existing query
    Then The page should return a table of members, each of which contains the query string in the info

    Examples:
      | browser  |
      |    .     |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_10 - Verify searching members using a non-existing query -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=medium
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Search Member page
    When The user searches with a non-existing query
    Then The page should return an empty table

    Examples:
      | browser  |
      |    .     |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_11 - Verify searching members without input the query -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=low
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the Search Member page
    When The user clicks on the search button without input the query
    Then The search function should not work

    Examples:
      | browser  |
      |    .     |

  @regression
  Scenario Outline: WEB_ProfMgr_Func_12 - Verify viewing a member info using a non-existing ID -- <browser>
    """
    **Author**: Tai Thai
    **Reviewer**: Tai Thai
    **Created date**: 15/01/2023
	**Latest updates**: N/A

    priority=low
    min_version=0.0.1
    max_version=None
    """
    Given The user accesses the View Member page
    When The user searches with a non-existing ID
    Then The page should return nothing

    Examples:
      | browser  |
      |    .     |
