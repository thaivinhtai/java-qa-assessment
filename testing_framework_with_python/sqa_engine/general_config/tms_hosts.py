#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module is used to define the Test Management System host. There are 2
variables: ISSUE_PATTERN and LINK_PATTERN
"""

# This is used to link the bugs, issue, epic....
ISSUE_PATTERN = "https://gitlab.com/thaivinhtai/java-qa-assessment/-/issues/{}"

# This can be used to link to the test cases document...
LINK_PATTERN = "https://tai-thai-demo.notion.site/{}"
