#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module module handles python script generating.
"""

from sqa_engine.utilities.test_suites_management import get_test_suite_path


def __get_feature_scenarios_name(feature_file_content: list) -> list:
    """

    """
    # Generate a temp list that store line index of test cases name.
    list_test_cases = list()

    # Add test case's index to temp list.
    for index, line in enumerate(feature_file_content):
        if line.strip().rstrip().startswith('Scenario:') or \
                line.strip().rstrip().startswith('Scenario Outline'):
            test_case_name = line.strip().rstrip().split(":")[-1]
            list_test_cases.append(
                f"        self.__list_test_cases"
                f".append('{test_case_name.strip().rstrip()}')" + '\n')
    return list_test_cases


def generate_sequence(test_module: str, test_design: str) -> None:
    """Generate sequence.

    This module rewrites the sequence of test suite base on the moderation of
    robot file. It will update the sequence_{test_design_name}.py

    Parameters
    ----------
    test_module : str
        Name of test module {mobile_app, web_portal, rest_api}
    test_design : str
        Test domain specification

    Returns
    -------
    None
    """
    extension = "feature"
    _, test_suite, path_to_test_suite, destination_suite = \
        get_test_suite_path(test_module=test_module, test_design=test_design)
    sequence_file = \
        f'{test_suite}/{path_to_test_suite}sequence_{destination_suite}.py'
    test_case_file = \
        f'{test_suite}/{path_to_test_suite}{destination_suite}.{extension}'

    # Generate empty list to store names of test cases.
    list_test_cases = list()

    # Read content on Robot file.
    with open(test_case_file, 'r') as file_content:
        contents = file_content.readlines()
        list_test_cases += __get_feature_scenarios_name(contents)

    list_test_cases.sort()

    # Generate list that stores content of header and footer of sequence file.
    start_of_sequence_file = list()
    end_of_sequence_file = ['\n']

    # Open sequence file to get header and footer content.
    with open(sequence_file, 'r') as sequence_file_content:
        contents = sequence_file_content.readlines()
        temp_index = int()

        # Get header content.
        for index, line in enumerate(contents):
            start_of_sequence_file.append(line)
            if line.find('self.__list_test_cases = list()') != -1:
                temp_index = index
                break

        for index, line in enumerate(contents[temp_index:]):
            if line.find('@property') != -1:
                temp_index += index
                break

        # Get footer content.
        for line in contents[temp_index:]:
            end_of_sequence_file.append(line)

    # Full content to rewrite sequence file.
    contents_to_be_written =\
        start_of_sequence_file + list_test_cases + end_of_sequence_file

    # Rewrite sequence file.
    with open(sequence_file, 'w') as sequence:
        sequence.writelines(contents_to_be_written)
    sequence.close()

    print(*contents_to_be_written)
