#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains function that generates log folders.
"""

from datetime import datetime
from os import mkdir, path
from sqa_engine.utilities.workspace_vars import InternalPath
from sqa_engine.utilities.executor import create_empty_file
from sqa_engine.utilities.args_handler.args_parser import ARGUMENTS
from threading import current_thread


TODAY = datetime.now().strftime("%Y-%m-%d")
CURRENT_TIME = datetime.now().strftime("%H-%M-%S")


def generate_current_time_execution_log_dir(module_name: str) -> None:
    """Create and return path of folder that stores current time log files.

    This function takes the timestamp when test cases are run, then create
    directories to store log files. The directory is under module and its name
    is the current time (hh-mmm-ss).

    The structure of generated folders:
        workspace/{today}/{module}/{current_time}

    Parameters
    ----------
    module_name : str
        The name of module to be tested {REST, web, app}.

    Returns
    -------
    None
    """
    today = TODAY
    current_time = CURRENT_TIME
    logs_path = InternalPath.logs_folder

    today_log_folder = f'{logs_path}/{today}'
    module_log_folder = f'{today_log_folder}/{module_name}'
    current_time_execution_log_dir = f'{module_log_folder}/{current_time}'

    for directory in (logs_path, today_log_folder,
                      module_log_folder, current_time_execution_log_dir):
        if not path.exists(directory):
            try:
                mkdir(directory)
            except FileExistsError:
                continue

    InternalPath.current_time_execution_log_dir = \
        current_time_execution_log_dir


def generate_test_suite_logs_folder(test_design: str) -> None:
    """Create and return path of folder that stores log files.

    This function takes the timestamp when test cases are run, then create
    directories to store log files. The directories are classified by Module,
    TDS.

    The structure of generated folders:
        workspace/{today}/{module}/{current_time}/{test_design}

    Parameters
    ----------
    test_design : str
        Test Domain Specification.

    Returns
    -------
    None
    """
    path_to_test_design = test_design.split('/')
    test_design = path_to_test_design[-1]
    path_to_test_design.remove(test_design)

    temp_path = InternalPath.current_time_execution_log_dir

    for folder in path_to_test_design:
        temp_path = f'{temp_path}/{folder}'
        if not path.exists(f'{temp_path}'):
            mkdir(temp_path)

    executing_log_folder = f'{temp_path}/{test_design}'
    allure_result_folder = f'{executing_log_folder}/allure-results'
    screenshot_folder = f'{executing_log_folder}/screenshots'

    if not path.exists(executing_log_folder):
        mkdir(executing_log_folder)
        mkdir(allure_result_folder)
        mkdir(screenshot_folder)

    # update path to InternalPath instance attribute.
    InternalPath.current_allure_result_folder.append(allure_result_folder)
    InternalPath.current_log_folder = executing_log_folder
    InternalPath.current_screenshot_folder = screenshot_folder
