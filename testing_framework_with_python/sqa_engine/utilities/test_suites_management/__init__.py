#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This package contains modules that relates to test suites management.
"""

from .test_suite_retriever import get_module_children, get_test_suite_path,\
    get_list_test_suite, convert_sequence_to_camel_case
from .test_suite_switcher import switch_to_tds
