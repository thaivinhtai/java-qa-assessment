#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

from os import environ


__ENVIRONMENT = environ['ENVIRONMENT'].replace('.', '_', 2)

__IMPORT_PACKAGE__ = f'sqa_resource.environments.{__ENVIRONMENT}'

__PAGE__ = __import__(__IMPORT_PACKAGE__, globals(), locals(),
                      ['WEB_URL', 'API_HOST', 'API_PREFIX'], 0)

WEB_URL = __PAGE__.WEB_URL
API_HOST = __PAGE__.API_HOST
API_PREFIX = __PAGE__.API_PREFIX
