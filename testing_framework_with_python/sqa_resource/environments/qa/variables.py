"""
This module stores the test data.
"""

WEB_URL = "http://localhost:8080/qa/app"
API_HOST = "http://localhost:3001"
API_PREFIX = "/qa/api"
