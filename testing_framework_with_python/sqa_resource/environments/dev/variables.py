"""
This module stores the test data.
"""

WEB_URL = "http://localhost:8080/dev/app"
API_HOST = "http://localhost:3002"
API_PREFIX = "/dev/api"
