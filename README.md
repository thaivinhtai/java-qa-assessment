# Tai Thai - Automation QA

## The Automation Testing Framework With JAVA

The detailed document can be found [here](testFramework/README.md).

## The Automation Testing Framework With Python

Actually, I'm not good at Java, so I put the Framework using Python here for reference. The detailed document can be found [here](testing_framework_with_python/README.md).

## Getting started

Make sure your machine has already been installed with [Git](https://git-scm.com/).

Use the following commands to clone the project:

```
git clone https://gitlab.com/thaivinhtai/java-qa-assessment.git
```

## Configure the testing environments

The following steps were tested on macOS Ventura 13.0. However, those might run well on Debian-based distros.

1. Pre-re:
   - [Docker](https://www.docker.com/)
2. Deploy the needed services:
    ```
    cd java-qa-assessment
    cd app && ./start_all
    ```
   The following services will be deployed:
   - Environment Portal - Nginx: http://localhost:8080/
   - Web App - QA Env: http://localhost:8080/qa/app
   - API - QA Env: http://localhost:8080/qa/api/
   - Web App - Dev Env: http://localhost:8080/dev/app
   - API - Dev Env: http://localhost:8080/dev/api/
   - Allure Report Dashboard: http://localhost:8080/allure/
   - Allure API: http://localhost:8080/allure/api

## Run the Java Test

1. Pre-re:
   - [Maven](https://maven.apache.org/)
   - [OpenJDK 17](https://openjdk.org/projects/jdk/17/)
2. To setup dependencies, run the following command in java-qa-assessment directory:
   ```shell
   mvn clean install
   ```
3. To run the tests, run the following command in java-qa-assessment directory:
   ```shell
   mvn test "-Dargs=--testMatch profile_manager.feature --debug --pushResult test"
   ```

Then you can view the report on Allure [here,](http://localhost:8080/allure/allure-docker-service/projects/test/reports/latest/index.html)
or you can visit the Allure general dashboard: http://localhost:8080/allure/allure-docker-service-ui/

## Clean up the testing environments

To clean up the environments, execute the following script:

```shell
./app/clean_up
```
