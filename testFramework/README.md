# Automation Testing Framework With Java

## Table of Contents
- [Assessment overview](#Assessment overview)
  - [Overall test automation framework design](#Overall test automation framework design)
  - [Ability to run the test scripts across multiple environments](#Ability to run the test scripts across multiple environments)
  - [Test reporting capability](#Test reporting capability)
  - [Test scenarios coverage](#Test scenarios coverage)
  - [Bugs/enhancements identified](#Bugs/enhancements identified)
- [Automation Framework Usage](#Automation Framework Usage)
  - [Running in Debug Mode](#Running in Debug Mode)
  - [Push result to Allure report server](#Push result to Allure report server)

## Assessment overview
### Overall test automation framework design

The automation framework has the following overall structure:

```
root_dir
    |
    |----- poom.xml
    |
    |----- testFramework
                |
                |
                |---------- sqaCore
                |              |
                |              |-------- TestRunner                             # The process starts here.
                |              |
                |              |-------- libs
                |                          |
                |                          |
                |                          |-------- TestCasesCucumberRunner     # Handle running config
                |                          |
                |                          |-------- config.json                 # In the future, maybe you want to implement some interesting stuffs, that needs some config
                |                          |
                |                          |-------- common                      # All libs to manage the framework, include: generating logs, push result,
                |                          |
                |                          |-------- autoLibs                    # Low-level automation libs, which can be reusable
                |
                |
                |----- sqaApiTest          
                |          |
                |          |----- libs                    # High-level libs for specified test module, for ex: testStepDefinitions
                |          |----- test_design             # Test cases are stored here
                |          
                |----- sqaResource                        # Common test data stored here          
                |          |
                |          |-------- env                  # This package deals with multiple env
                |          
                |          
                |----- <packages just like the sqaWebTest if the project scales out>          
```


### Ability to run the test scripts across multiple environments

By using the properties file, we can easily read the file by changing path and parse the data easily with Java. The following is the sqaResource/env structure.

```
env
 |-------- dev.properties
 |-------- qa.propties
```

### Test reporting capability

The framework supports run Allure report locally as well as push the report to the online server.

### Test scenarios coverage

Please visit my Notion for the [Trace-matrix](https://tai-thai-demo.notion.site/Trace-matrix-5b5eac73ccf94fdca0cc914b88c77e47) and the [test list](https://tai-thai-demo.notion.site/80311a8260e04594963b707a70632321?v=4ead37b91d944d74a5aaa96a8768b7dd).

These two pages are linked to the Allure Report.
   

## Automation Framework Usage
### Quick start
```shell
mvn test "-Dargs=--testMatch"
```

### Running in Debug Mode

By adding the '--debug' flag, the scripts will run in debug mode. This means the logging is more detailed, and the web will run in UI mode. By default, the web will run in headless mode.

For example:
```shell
mvn test "-Dargs=--testMatch profile_manager.feature --debug"
```

### Push result to Allure report server

If you run the setup script in the app/start_all, the Allure URL should be: http://localhost:8080/allure/allure-docker-service-ui

1. Access the Allure server and create a project
2. Push the testing result to the project with the following command:
   ```shell
    mvn test "-Dargs=--testMatch profile_manager.feature --debug --pushResult <projectId>"
   ```
