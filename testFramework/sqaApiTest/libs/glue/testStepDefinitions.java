package sqaApiTest.libs.glue;

import io.cucumber.java.BeforeAll;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import libs.autoLibs.ApiUtil;
import libs.autoLibs.EnvUtil;
import libs.autoLibs.JsonUtil;
import libs.common.FrameworkLogger;
import libs.common.PropertiesFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;


public class testStepDefinitions  {

    static String testEnvironment = EnvUtil.getEnvironment();
    static String currentDir = System.getProperty("user.dir");
    static Properties data = PropertiesFile.read(
            currentDir + "/testFramework/sqaResource/env/" + testEnvironment + ".properties"
    );
    String statusCode;
    String message;
    Map<String, Object> mapObject;

    public testStepDefinitions() throws ClassNotFoundException {}

    @BeforeAll
    public static void beforeAll() throws IOException {
        String[] header = new String[] {"Content-Type", "application/json"};
        Map<String, String> body = new HashMap<>();

        Map<String, Object> response = ApiUtil.sendRequestAndReceiveResponse(
                "GET", data.getProperty("API_HOST") + data.getProperty("API_PREFIX") + "/members",
                header, body
        );
        List<Map<String, Object>> memberFromAPI = JsonUtil.JsonToList.parse((String) response.get("message"));
        memberFromAPI.forEach(
                member -> {
                    try {
                        ApiUtil.sendRequestAndReceiveResponse(
                                "DELETE",
                                data.getProperty("API_HOST") + data.getProperty("API_PREFIX")
                                        + "/members/" + member.get("id").toString(),
                                header, body
                                );
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
        );

        List<Map<String, Object>> existingMembers =
                JsonUtil.JSONFile.parseList(currentDir + "/testFramework/sqaResource/existing_members.json");
        existingMembers.forEach(
                member -> {
                    try {
                        ApiUtil.sendRequestAndReceiveResponse(
                                "POST",
                                data.getProperty("API_HOST") + data.getProperty("API_PREFIX") + "/members",
                                header, member
                        );
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
    }

    @Given("The client send a GET request to the members api")
    public void theClientSendAGETRequestToTheMembersApi() throws IOException {
        String[] header = new String[] {"Content-Type", "application/json"};
        Map<String, String> body = new HashMap<>();
        this.mapObject = ApiUtil.sendRequestAndReceiveResponse(
                "GET", data.getProperty("API_HOST") + data.getProperty("API_PREFIX") + "/members",
                header, body
        );
    }

    @When("The client gets the response from the server")
    public void theClientGetsTheResponseFromTheServer() {
        this.statusCode = this.mapObject.get("status_code").toString();
        this.message = this.mapObject.get("message").toString();
    }

    @Then("The list members data should be equal with the data from {string}")
    public void theListMembersDataShouldBeEqualWithTheDataFrom(String arg0) throws IOException {
        List<Map<String, Object>> existingMembers =
                JsonUtil.JSONFile.parseList(currentDir + "/testFramework/" + arg0);
        List<Map<String, Object>> memberFromAPI = JsonUtil.JsonToList.parse(this.message);

        assert existingMembers.size() == memberFromAPI.size();
        Set<?> set1 = new HashSet<>(existingMembers);
        Set<?> set2 = new HashSet<>(memberFromAPI);
        assert set1.equals(set2);
    }

    @And("The status code should be {string}")
    public void theStatusCodeShouldBe(String arg0) {
        assert this.statusCode.equalsIgnoreCase(arg0);
    }

    @Given("The client send a POST request to the members api to create a new member with data from {string}")
    public void theClientSendAPOSTRequestToTheMembersApiToCreateANewMemberWithDataFrom(String arg0) throws IOException {
        String[] header = new String[] {"Content-Type", "application/json"};
        Map<String, Object> body = JsonUtil.JSONFile.parseMap(currentDir + "/testFramework/" + arg0);
        this.mapObject = ApiUtil.sendRequestAndReceiveResponse(
                "POST", data.getProperty("API_HOST") + data.getProperty("API_PREFIX") + "/members",
                header, body
        );
    }

    @Then("The new member info should be equal with the data from {string}")
    public void theNewMemberInfoShouldBeEqualWithTheDataFrom(String arg0) throws IOException {
        Map<String, Object> dataFromFile = JsonUtil.JSONFile.parseMap(currentDir + "/testFramework/" + arg0);

        String[] header = new String[] {"Content-Type", "application/json"};
        Map<String, String> body = new HashMap<>();
        this.mapObject = ApiUtil.sendRequestAndReceiveResponse(
                "GET", data.getProperty("API_HOST")
                        + data.getProperty("API_PREFIX") + "/members/" + this.message,
                header, body
        );
        this.message = this.mapObject.get("message").toString();

        Map<String, Object> dataFromApi = JsonUtil.JsonToMap.parse(this.message);
        dataFromApi.remove("id");

        assert dataFromApi.equals(dataFromFile);

    }
}
