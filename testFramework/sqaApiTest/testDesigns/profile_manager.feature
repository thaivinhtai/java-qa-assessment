Feature: Profile Manager

  Scenario: Verify the list of members
    Given The client send a GET request to the members api
    When The client gets the response from the server
    Then The list members data should be equal with the data from "sqaResource/existing_members.json"
      And The status code should be "200"

  Scenario: Verify adding a new member
    Given The client send a POST request to the members api to create a new member with data from "sqaResource/new_member.json"
    When The client gets the response from the server
    Then The new member info should be equal with the data from "sqaResource/new_member.json"
      And The status code should be "200"
