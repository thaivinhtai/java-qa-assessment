// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.

import libs.autoLibs.EnvUtil;
import libs.common.ArgsHandler;
import libs.TestCasesCucumberRunner;
import net.sourceforge.argparse4j.inf.Namespace;

import java.io.IOException;
import java.util.List;


public class TestRunner {
    public static void main(String[] args) throws IOException {
        Namespace arguments = ArgsHandler.getArgs(args);

        String module = arguments.getString("module");
        String environment = arguments.getString("environment");
        String allureProjectId = arguments.getString("pushResult");
        List<Object> testMatch = arguments.getList("testMatch");
        boolean debug = arguments.getBoolean("debug");

        EnvUtil.setEnvironment(environment);

        TestCasesCucumberRunner runner = new TestCasesCucumberRunner(
                module, testMatch, allureProjectId, debug
        );
        runner.run();
    }
}