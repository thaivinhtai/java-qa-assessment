package libs.common;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import io.qameta.allure.Allure;

class CustomLogger {

    public static final String RESET = "\033[0m";  // Text Reset

    public static final String GREEN = "\033[0;32m";   // GREEN
    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static final String BLUE = "\033[0;34m";    // BLUE

    private String logLevel;
    private String executionLogFile;

    public CustomLogger(String logLevel) {
        this.logLevel = logLevel;
    }

    public CustomLogger() {
        this.logLevel = "INFO";
    }

    public void addLogFile(String filePath) {
        this.executionLogFile = filePath;
    }

    public void updateLoglevel(String level) {
        this.logLevel = level;
    }

    private String getCurrentTime() {
        LocalDateTime now = LocalDateTime.now();
        String currentTime = now.format(DateTimeFormatter.ofPattern("HH-mm-ss"));
        String today = now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        return today + " " + currentTime;
    }

    private String logInfo() {
        return "\u001B[32mINFO\u001B[0m";
    }

    private String logWarning() {
        return "\u001B[33mWARN\u001B[0m";
    }

    public void log(String level, String message) {
        String fullMessage = "";
        if (this.logLevel.equalsIgnoreCase("info")) {
            if (this.logLevel.equalsIgnoreCase("info")) {
                fullMessage = " " + this.getCurrentTime() + " - " + this.logInfo() + " - "  + message;
                System.out.println(fullMessage);
            }
        } else {
            if (level.equalsIgnoreCase("info")) {
                fullMessage = " " + this.getCurrentTime() + " - " + this.logInfo() + " - "  + message;
                System.out.println(fullMessage);
            }
            if (level.equalsIgnoreCase("warn")) {
                fullMessage = " " + this.getCurrentTime() + " - " + this.logWarning() + " - "  + message;
                System.out.println(fullMessage);
            }
            if (level.equalsIgnoreCase("debug")) {
                fullMessage = " " + this.getCurrentTime() + " - DEBUG - "  + message;
                System.out.println(fullMessage);
            }
        }
        if (this.logLevel.equalsIgnoreCase("debug")) {
            Allure.addAttachment(null, "text/plain", fullMessage, null);
        }

        try {
            FileWriter fileWriter = new FileWriter(this.executionLogFile, true);
            fileWriter.write("\n" + fullMessage);
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}


public class FrameworkLogger {

    private static final CustomLogger LOGGER = new CustomLogger();

    public static void init(String level) {
        LOGGER.updateLoglevel(level);
    }

    public static void setLogFile(String filePath) {
        LOGGER.addLogFile(filePath);
    }

    public static void log(String level, String message) {
        LOGGER.log(level, message);
    }

}
