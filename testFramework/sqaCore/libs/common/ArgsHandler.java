package libs.common;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.util.Arrays;

import static net.sourceforge.argparse4j.impl.Arguments.storeTrue;

public class ArgsHandler {
    public static String[] appendToArray(String[] arr, String element) {
        String[] elementArray;
        if (element == null) {
            return arr;
        }
        if (element.contains(" ")) {
            elementArray = element.split("\\s+");
        } else {
            elementArray = new String[] {element};
        }
        String[] newArr = Arrays.copyOf(arr, arr.length + elementArray.length);
        System.arraycopy(elementArray, 0, newArr, arr.length, elementArray.length);
        return newArr;
    }
    public static Namespace getArgs(String[] args) {
        ArgumentParser parser = ArgumentParsers.newFor("Main").build()
                .defaultHelp(true)
                .description("A simple demo of Hybrid Testing Framework");

        parser.addArgument("-m", "--module")
                .type(String.class)
                .dest("module")
                .setDefault("api")
                .help("Module to test: api | web");

        parser.addArgument("-t", "--testMatch")
                .nargs("*")
                .dest("testMatch")
                .help("Feature files to run");

        parser.addArgument("-e", "--environment")
                .type(String.class)
                .dest("environment")
                .setDefault("qa")
                .help("Specify the environment to be tested: qa | dev");

        parser.addArgument("-b", "--browser")
                .type(String.class)
                .dest("browser")
                .setDefault("chrome")
                .help("Browser to be run tests on: chrome | firefox | edge");

        parser.addArgument("-p", "--pushResult")
                .type(String.class)
                .dest("pushResult")
                .help("Push result to allure report server");

        parser.addArgument("-d", "--debug")
                .action(storeTrue())
                .dest("debug")
                .help("Print debug log when running");

        String[] arguments = new String[]{};
        if (args != null) {
            for (String element : args) {
                arguments = ArgsHandler.appendToArray(arguments, element);
            }
        }

        try {
            return parser.parseArgs(arguments);
        } catch (Exception e) {
            assert e instanceof ArgumentParserException;
            if (e.getMessage().contains("because \"this.text\" is null")) {
                System.exit(0);
            }
            parser.handleError((ArgumentParserException) e);
            System.exit(1);
        }
        return null;
    }
}
