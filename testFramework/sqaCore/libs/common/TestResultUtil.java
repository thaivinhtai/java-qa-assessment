package libs.common;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.URISyntaxException;

import libs.autoLibs.JsonUtil.JSONFile;

public class TestResultUtil {

    private static String[] getAllureHostAndPrefix() throws IOException {

        String configFile = System.getProperty("user.dir") + "/testFramework/sqaCore/libs/config.json";
        JSONFile config = new JSONFile(configFile);

        String allureHost = config.get("allure_server");
        String prefixUrl = config.get("allure_prefix_url");
        return new String[] {allureHost, prefixUrl};

    }

    public static void pushResult(String projectId, String resultPath) throws IOException {

        System.out.println("------------------------------------------------------------------");
        System.out.println("RESULTS DIRECTORY PATH: " + resultPath);
        System.out.println("PUSH RESULT TO: " + projectId);
        System.out.println("------------------------------------------------------------------");

        String[] allureServerInfo = TestResultUtil.getAllureHostAndPrefix();
        String serverAddress = allureServerInfo[0];
        String serverAddressPrefixUrl = allureServerInfo[1];
        String sendResultsEndpoint = serverAddressPrefixUrl + "/send-results";

        File directory = new File(resultPath);
        File[] files = directory.listFiles();
        List<Result> results = new ArrayList<>();

        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    Path path = Paths.get(file.getPath());
                    try {
                        byte[] content = Files.readAllBytes(path);
                        if (content.length != 0) {
                            String base64Content = Base64.getEncoder().encodeToString(content);
                            Result result = new Result();
                            result.setFileName(file.getName());
                            result.setContentBase64(base64Content);
                            results.add(result);
                        } else {
                            System.out.println("Empty File skipped: " + file.getPath());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("Directory skipped: " + file.getPath());
                }
            }
        }

        RequestBody requestBody = new RequestBody();
        requestBody.setResults(results);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonRequestBody = gson.toJson(requestBody);

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest request = null;
        try {
            URI uri = new URI(serverAddress +  sendResultsEndpoint + "?project_id=" + projectId);
            request = HttpRequest.newBuilder()
                    .uri(uri)
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(jsonRequestBody))
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        HttpResponse<String> response = null;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        if (response != null) {
            System.out.println("STATUS CODE:");
            System.out.println(response.statusCode());
            System.out.println("RESPONSE:");
            System.out.println(response.body());
        }
    }

    private static class RequestBody {
        private List<Result> results;

        public List<Result> getResults() {
            return results;
        }

        public void setResults(List<Result> results) {
            this.results = results;
        }
    }

    private static class Result {
        private String file_name;
        private String content_base64;

        public String getFileName() {
            return file_name;
        }

        public void setFileName(String fileName) {
            this.file_name = fileName;
        }

        public String getContentBase64() {
            return content_base64;
        }

        public void setContentBase64(String contentBase64) {
            this.content_base64 = contentBase64;
        }
    }
}
