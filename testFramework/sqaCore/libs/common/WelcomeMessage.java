package libs.common;

import com.github.lalyos.jfiglet.FigletFont;

import java.io.*;

public class WelcomeMessage {

    public static void printMessage(String module, String logFile) throws IOException {
        try {
            File file = new File("log.txt");
            FileOutputStream fos = new FileOutputStream(logFile);

            PrintStream ps = new PrintStream(fos);
            PrintStream consoleStream = System.out;

            System.setOut(ps);

            System.out.println("------------------------------------" +
                    "----------------------------------------------------------------------");
            String asciiArt = FigletFont.convertOneLine("App Test - " + module.toUpperCase());
            System.out.println(asciiArt);
            System.out.println("------------------------------------" +
                    "----------------------------------------------------------------------");

            System.setOut(consoleStream);
            ps.close();
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("------------------------------------" +
                "----------------------------------------------------------------------");
        String asciiArt = FigletFont.convertOneLine("App Test - " + module.toUpperCase());
        System.out.println(asciiArt);
        System.out.println("------------------------------------" +
                "----------------------------------------------------------------------");
    }
}
