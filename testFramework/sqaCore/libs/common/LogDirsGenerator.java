package libs.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.io.File;
import java.io.IOException;

public class LogDirsGenerator {

    public static String generateCurrentTimeExecutionLogDir(String module) throws IOException {

        LocalDateTime now = LocalDateTime.now();
        String currentTime = now.format(DateTimeFormatter.ofPattern("HH-mm-ss"));
        String today = now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String currentDir = System.getProperty("user.dir");
        return currentDir + File.separator + "testFramework" + File.separator + "artifacts"
                + File.separator + today + File.separator + module
                + File.separator + currentTime;
    }

}
