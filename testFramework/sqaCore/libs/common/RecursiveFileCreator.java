package libs.common;

import java.io.File;
import java.io.IOException;

public class RecursiveFileCreator {

    public static void createFile(String filePath) throws IOException {
        File file = new File(filePath);

        // If parent directories do not exist, create them recursively
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        if (!file.exists()) {
            // Create the file
            file.createNewFile();
        }
    }

}
