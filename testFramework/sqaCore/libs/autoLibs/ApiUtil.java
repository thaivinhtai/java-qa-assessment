package libs.autoLibs;

import io.cucumber.core.internal.com.fasterxml.jackson.core.JsonProcessingException;
import io.cucumber.core.internal.com.fasterxml.jackson.databind.ObjectMapper;
import libs.common.FrameworkLogger;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

public class ApiUtil {

    public static String makeBeautyJson(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    }

    public static Map<String, Object> sendRequestAndReceiveResponse(
            String method, String uri, String[] headers, Object body
    ) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String beautyHeaders;
        String beautyBody;

        try {
            beautyHeaders = makeBeautyJson(headers);
        } catch (JsonProcessingException e) {
            beautyHeaders = "{}";
            FrameworkLogger.log("warn",
                    "Failed to make beauty headers JSON: " + e.getMessage());
        }

        try {
            beautyBody = makeBeautyJson(body);
        } catch (JsonProcessingException e) {
            beautyBody = "{}";
            FrameworkLogger.log("warn",
                    "Failed to make beauty body JSON: " + e.getMessage());
        }

        FrameworkLogger.log("debug","Request Headers: \n" + beautyHeaders);
        FrameworkLogger.log("debug","Request data: \n" + beautyBody);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .headers(headers)
                .method(method, body == null ? HttpRequest.BodyPublishers.noBody() :
                        HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(body)))
                .build();

        HttpResponse<String> response;

        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            FrameworkLogger.log("error","Error sending HTTP request: " + e.getMessage());
            throw new RuntimeException(e);
        }

        String responseBody = response.body();
        int statusCode = response.statusCode();

        FrameworkLogger.log("debug","Got response message: \n" + responseBody);
        FrameworkLogger.log("info","Status code: " + statusCode);

        return Map.of("message", responseBody, "status_code", statusCode);
    }
}
