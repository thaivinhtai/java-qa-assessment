package libs.autoLibs;

public class EnvUtil {

    private static String environment;

    public static void setEnvironment(String value) {
        environment = value;
    }

    public static String getEnvironment() {
        return environment;
    }

}
