package libs.autoLibs;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JsonUtil {

    public static class JSONFile {
        private final String jsonFilePath;
        private JsonObject jsonObject;
        public JSONFile(String jsonFilePath) throws FileNotFoundException {
            this.jsonFilePath = jsonFilePath;
            this.parseJson(this.jsonFilePath);
        }

        private void parseJson(String jsonFilePath) throws FileNotFoundException {
            FileReader reader = new FileReader(this.jsonFilePath);
            this.jsonObject = JsonParser.parseReader(reader).getAsJsonObject();
        }

        public String get(String property) {
            String value = jsonObject.get(property).toString();
            return value.substring(1, value.length() -1);
        }

        public static Map<String, Object> parseMap(String filePath) throws FileNotFoundException {
            File jsonFile = new File(filePath);
            BufferedReader reader = new BufferedReader(new FileReader(jsonFile));
            String json = reader.lines().collect(Collectors.joining());
            Gson gson = new Gson();
            return gson.fromJson(json, new TypeToken<Map<String, Object>>(){}.getType());
        }

        public static List<Map<String, Object>> parseList(String filePath) throws IOException {
            String json = new String(Files.readAllBytes(Paths.get(filePath)));
            Gson gson = new Gson();
            Type type = List.class;
            return gson.fromJson(json, type);
        }
    }

    public static class JsonToMap {

        public static Map<String, Object> parse(String jsonString) {
            Gson gson = new Gson();
            Type type = Map.class;
            return gson.fromJson(jsonString, type);
        }
    }

    public static class JsonToList {

        private static final Gson gson = new Gson();

        public static List<Map<String, Object>> parse(String jsonString) {
            Type listType = new com.google.gson.reflect.TypeToken<List<Map<String, Object>>>() {}.getType();
            return gson.fromJson(jsonString, listType);
        }
    }

}
