package libs;

import io.cucumber.core.cli.Main;
import libs.common.LogDirsGenerator;
import libs.common.WelcomeMessage;
import libs.common.FrameworkLogger;
import libs.common.TestResultUtil;
import libs.common.RecursiveFileCreator;
import org.apache.log4j.BasicConfigurator;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestCasesCucumberRunner {
    private final String module;
    private final List<Object> testMatch;
    private final String allureProjectId;
    private final boolean debug;
    private String glue;

    public TestCasesCucumberRunner(
            String module, List<Object> testMatch, String allureProjectId, boolean debug
    ) {
        this.module = module;
        this.testMatch = testMatch;
        this.glue = "libs.glue";
        this.allureProjectId = allureProjectId;
        this.debug = debug;
    }

    private void updateTestMatchAndGlue() {
        String module;
        if (this.module.equalsIgnoreCase("api")) {
            module = "sqaApiTest";
        } else {
            module = "sqaWebTest";
        }
        try {
            this.testMatch.replaceAll(o -> "testFramework" + File.separator + module
                    + File.separator + "testDesigns" + File.separator + o);
            this.glue = module + "." + this.glue;
        } catch (Exception e) {
            if (e.getMessage().contains("because \"this.testMatch\" is null")) {
                System.exit(0);
            }
            throw e;
        }
    }

    public void run() throws IOException {
        String logDir = LogDirsGenerator.generateCurrentTimeExecutionLogDir(this.module);
        String allureResultPath = logDir + File.separator + "allure-results";
        String executionLogFile = logDir + File.separator + "execution.log";

        this.updateTestMatchAndGlue();

        StringBuilder featureFilesExpressions = new StringBuilder();
        for (Object expression : this.testMatch) {
            featureFilesExpressions.append(expression).append(" ");
        }

        RecursiveFileCreator.createFile(executionLogFile);
        FrameworkLogger.setLogFile(executionLogFile);
        if (this.debug) {
            FrameworkLogger.init("debug");
        }

        BasicConfigurator.configure();
        System.setProperty("log4j.rootLogger", "INFO, console");
        System.setProperty("log4j.appender.console", "org.apache.log4j.ConsoleAppender");
        System.setProperty("log4j.appender.console.Target", "System.out");
        System.setProperty("log4j.appender.console.layout", "org.apache.log4j.PatternLayout");
        System.setProperty("log4j.appender.console.layout.ConversionPattern", "%-4r [%t] %-5p %c %x - %m%n");

        System.setProperty("allure.results.directory", allureResultPath);

        String[] featureFiles = String.valueOf(featureFilesExpressions).split("\\s+");
        String[] argv = {
                "--glue", this.glue,
                "--plugin", "pretty",
                "--plugin", String.format("html:%s/reports.html", logDir),
                "--plugin", String.format("junit:%s/report.xml", logDir),
                "--plugin", String.format("json:%s/report.json", logDir),
                "--plugin", "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm"
        };

        String[] arguments = new String[featureFiles.length + argv.length];
        System.arraycopy(featureFiles, 0, arguments, 0, featureFiles.length);
        System.arraycopy(argv, 0, arguments, featureFiles.length, argv.length);

        ClassLoader classLoader = TestCasesCucumberRunner.class.getClassLoader();

        WelcomeMessage.printMessage(this.module, executionLogFile);
        Main.run(arguments, classLoader);

        if (this.allureProjectId != null) {
            TestResultUtil.pushResult(this.allureProjectId, allureResultPath);
        }
    }
}